<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{ asset('assets/semantic.min.css') }}" rel="stylesheet" type="text/css">
@yield('styles')
</head>
<body>
@yield('content')
    <script src="{{ asset('assets/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/semantic.min.js') }}"></script>
@yield('scripts')
</body>
</html>