@extends('layouts.blank')
@section('title', 'Ferrari paddock')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui ferrari image header">
                <img src="{{ asset('assets/images/logo.png') }}" class="image">
                <span class="content">
                    paddock
                </span>
            </h2>
            <form action="{{ route('login') }}" method="post" class="ui large form">
                @csrf

                <div class="ui stacked segment">
                    <div class="{{ $errors->has('email') ? 'error ' : '' }}field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" placeholder="{{ trans('common.email') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('password') ? 'error ' : '' }}field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="{{ trans('common.password') }}">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large ferrari submit button">{{ trans('common.login') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('styles')
    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .column {
            max-width: 450px;
        }
    </style>
@endsection