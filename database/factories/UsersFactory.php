<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'display_name' => $faker->name,
        'user_name' => str_slug($faker->name),
        'remember_token' => str_random(10),
    ];
});
