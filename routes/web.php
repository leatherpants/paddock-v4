<?php

Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
    Route::post('login', ['uses' => 'LoginController@login']);
    Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::group(['middleware' => 'auth'], function () {
        Route::get('dashboard', ['uses' => 'DashboardController@dashboard', 'as' => 'dashboard']);
    });
});
