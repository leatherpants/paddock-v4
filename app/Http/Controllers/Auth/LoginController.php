<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\paddock\Users\Requests\LoginRequest;

class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Shows the login form.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handling for a login request.
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LoginRequest $request)
    {
        $details = $request->only('email', 'password');

        if (auth()->attempt($details)) {
            return redirect()
                ->route('dashboard');
        }

        return redirect()
            ->route('index');
    }

    /**
     * Handling for the logout request.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        auth()->logout();

        return redirect()
            ->route('index');
    }
}
